/*
Un alumno esta compuesto por:
-Un número de Control
-Un nombre completo
-Un grupo(clase)

Un grupo esta compuesto por
-Una clave
-Un nombre
-Una coleccion de alumnos(no_ de control)
-Una coleccion de materias(clave)?
deben de ser 7
-¿Una coleccion de profesores(clave)?
-Una coleccion de horarios
Horario esta compuesto por
-Clave
-Hora inicial
-Hora final
-Una materia()clave)
-Un profesor(clave)
-¿Un grupo(clave)?
Una materia esta compjuesta por
-Una clave
-Un nombre
Un profesor esta compuesto por
-Una clave
-Un nombre completo

*/

materias=[
    {
        "clave":"123qas","nombre":"biologia"
    },
    {
        "clave":"asd345","nombre":"matematicas"
    },
    {
        "clave":"678iuy","nombre":"quimica"
    },
    {
        "clave":"456tgb","nombre":"fisica"
    },
    {
        "clave":"518ñmb","nombre":"etica"
    },
    {
        "clave":"111ii8","nombre":"artes"
    },
    {
        "clave":"10kdhi","nombre":"educacion fisica"
    }
    ];

profesores=[
    {
        "clave":"hba","nombre_completo":"Hernandez Blas Antonio"
    },
    {
        "clave":"cbq","nombre_completo":"Claribel Benitez Quecha"
    },
    {
        "clave":"rncl","nombre_completo":"rogelio limon cordero"
    },
    {
        "clave":"marodmor","nombre_completo":"Miguel Angel Rodriguez"
    },
    {
        "clave":"ho","nombre_completo":"Hermez Ojeda"
    },
    {
        "clave":"cas","nombre_completo":"Clara Aurora Sanchez"
    },
    {
        "clave":"zlj","nombre_completo":"Jorge Zarate"
    }
    ];
    
    horarios=[
    {
        "clave":"1","hora_inicial":"07:00","hora_final":"08:00","materia":materias[0]["clave"],"profesor":profesores[0]["clave"]
    },
    {
        "clave":"2","hora_inicial":"08:00","hora_final":"09:00","materia":materias[1]["clave"],"profesor":profesores[1]["clave"]
    },
    {
        "clave":"3","hora_inicial":"09:00","hora_final":"10:00","materia":materias[2]["clave"],"profesor":profesores[2]["clave"]
    },
    {
        "clave":"4","hora_inicial":"10:00","hora_final":"11:00","materia":materias[3]["clave"],"profesor":profesores[3]["clave"]
    },
    {
        "clave":"5","hora_inicial":"11:00","hora_final":"12:00","materia":materias[4]["clave"],"profesor":profesores[4]["clave"]
    },
    {
        "clave":"6","hora_inicial":"12:00","hora_final":"13:00","materia":materias[5]["clave"],"profesor":profesores[5]["clave"]
    },
    {
        "clave":"7","hora_inicial":"13:00","hora_final":"14:00","materia":materias[6]["clave"],"profesor":profesores[6]["clave"]
    }    
        ];




alumnos=[
    {
    "no_control":"10160844",
    "nombre_completo":"Jimenez Alvarez Jasiel Efren",
    },
    {
    "no_control":"10161515",
    "nombre_completo":"Vasquez Pinacho Daniel",
    },
   {
    "no_control":"1016488",
    "nombre_completo":"Diaz Jimenez Magaly Polet",
    },
   {
    "no_control":"10184688",
    "nombre_completo":"Castellanos Rueda Edna",
    },
    {
    "no_control":"10160444",
    "nombre_completo":"Carreño Diaz Ruth Vilma",
    }
    ];
    
grupo={
    "clave":"001",
    "nombre":"ISA",
    "alumnos":alumnos,
    "materias":materias,
    "profesores":profesores,
    "horarios":horarios
    
};

/*Crear una funcion que dado un grupo, imprima una lista de alumnos*/
function imprimir_lista_alumnos(obj_grupo)
{
    for(var i=0;i<obj_grupo["alumnos"].length;i++)
    {
     console.log("Alumno "+i);
     console.log("No. de control: "+obj_grupo["alumnos"][i]["no_control"]);
     console.log("Nombre completo: "+obj_grupo["alumnos"][i]["nombre_completo"]);
    }
}
/*Crear una funcion que dado un grupo, imprima las relaciones horario-profesor-materia*/
function obtener_nombre_profesor(obj_profesor,clave_profesor)
{
    for(var i=0;i<obj_profesor.length;i++)
    {
        if(obj_profesor[i]["clave"]==clave_profesor)
        {return obj_profesor[i]["nombre_completo"];}
    }
    return "No existe profesor";
}

function obtener_nombre_materia(obj_materia,clave_materia)
{
    for(var i=0;i<obj_materia.length;i++)
    {
        if(obj_materia[i]["clave"]==clave_materia)
        {return obj_materia[i]["nombre"];}
    }
    return "No existe la materia";
}
function imprimir_lista_horarios(obj_grupo)
{
    for(var i=0;i<obj_grupo["horarios"].length;i++)
    {
     console.log("Horario "+i);
     var clavep=obj_grupo["horarios"][i]["profesor"];
     var clavem=obj_grupo["horarios"][i]["materia"];
     
     console.log(obj_grupo["horarios"][i]["hora_inicial"]+"-"+obj_grupo["horarios"][i]["hora_final"]+", Profesor: "+obtener_nombre_profesor(profesores,clavep)+", Materia: "+obtener_nombre_materia(materias,clavem));
    }
}
/*Crear funcion para buscar alumno en un grupo*/
function buscar_alumno_en_grupo(obj_grupo,no_control)
{
    for(var i=0;i<obj_grupo["alumnos"].length;i++)
    {
        if(obj_grupo["alumnos"][i]["no_control"]==no_control)
        {return true;}
    }
return false;
}
/*Crear funcion para buscar una materia en un grupo*/
function buscar_materia_en_grupo(obj_grupo,clave)
{
    for(var i=0;i<obj_grupo["materias"].length;i++)
    {
        if(obj_grupo["materias"][i]["clave"]==clave)
        {return true;}
    }
return false;
}
/*Crear una materia para buscar un profesor*/
function buscar_profesor_en_grupo(obj_grupo,clave)
{
    for(var i=0;i<obj_grupo["profesores"].length;i++)
    {
        if(obj_grupo["profesores"][i]["clave"]==clave)
        {return true;}
    }
return false;
}
/*Operaciones de CRUD en un grupo, insertar un nuevo alumno, profesor, materia
Funcion de insertar un alumno
*/
function insertar_alumno(obj_grupo,obj_alumno)
{
obj_grupo["alumnos"][obj_grupo["alumnos"].length]=obj_alumno;
}
/*Funciones para insertar una materia*/
function obtener_horario(obj_grupo,hora)
{
    for(var i=0;i<obj_grupo["horarios"].length;i++)
    {
        if(obj_grupo["horarios"][i]["hora_inicial"]==hora)
        {return i;}
    }
}
function insertar_materia(obj_grupo,obj_materia,horario)
{
    if(horario>"13:00"||horario<"07:00")
    {console.log("Horario fuera de lo permitido")
    return;
    }
 obj_grupo["materias"][obj_grupo["materias"].length]=obj_materia;
 obj_grupo["horarios"][obtener_horario(obj_grupo,horario)]["materia"]=obj_materia["clave"];
}
/*Funciones para insertar un profesor*/
function insertar_profesor(obj_grupo,obj_profesor,horario)
{
    if(horario>"13:00"||horario<"07:00")
    {console.log("Horario fuera de lo permitido")
    return;
    }
 obj_grupo["profesores"][obj_grupo["profesores"].length]=obj_profesor;
 obj_grupo["horarios"][obtener_horario(obj_grupo,horario)]["profesor"]=obj_profesor["clave"];
}
/*Funcion para eliminar profesor de un grupo*/
function delete_profesor(obj_grupo,clave_profesor)
{
    for(var j=0;j<obj_grupo["horarios"];j++)
    {
        if(obj_grupo["horarios"][j]["profesor"]==clave_profesor)
        {
           obj_grupo["horarios"][j]["profesor"]="Sin profesor asignado";
        }
    }
    
    for(var i=0;i<obj_grupo["profesores"];i++)
    {
        if(obj_grupo["profesores"][i]["clave"]==clave_profesor)
        {
            obj_grupo["profesores"].splice(i,1);
        }
    }
    
}
function delete_materia(obj_grupo,clave_materia)
{
    for(var i=0;i<obj_grupo["horarios"];i++)
    {
         if(obj_grupo["horarios"][i]["materia"]==clave_materia)
        {
           obj_grupo["horarios"][i]["materia"]="Sin materia asignada";
        }
    }
        for(var j=0;j<obj_grupo["materias"];j++)
    {
        if(obj_grupo["materias"][j]["clave"]==clave_materia)
        {
           obj_grupo["materias"].splice(j,1);
        }
    }
}
function delete_alumno(obj_grupo,clave_alumno)
{
    for(var i=0;i<obj_grupo["alumnos"];i++)
    {
         if(obj_grupo["alumnos"][i]["no_control"]==clave_alumno)
        {
           obj_grupo["alumnos"].splice(i,1);
        }
    }
}
/*En caso de delete que sea a alumno,profesor y materia si hay que borrar a un docente borrar al docente de tal hora, en materia borrar materia del grupo*/
/*Pruebas de las funciones*/
imprimir_lista_alumnos(grupo);  
imprimir_lista_horarios(grupo);
buscar_alumno_en_grupo(grupo,"10160844ff");
buscar_materia_en_grupo(grupo,"123qas");
buscar_profesor_en_grupo(grupo,"hba");
insertar_alumno(grupo,{"no_control":"14896652","nombre_completo":"Juanito banana"});
insertar_materia(grupo,{"clave":"1111","nombre":"sistemas programables"},"07:00");
insertar_profesor(grupo,{"clave":"1331","nombre_completo":"AMagaly Polet Diaz"},"07:00");
delete_profesor(grupo,"cbd");
delete_materia(grupo,"123qas");
delete_alumno(grupo,"10161515");